# Open your $EDITOR to compose a message in weechat
#
# Usage:
# /edit [extension]
# /fenced [extension]
#
# Optional settings:
# /set plugins.var.python.edit.editor "vim -f"
#
# History:
# Francisco Lopes
# Version 1.1.0:
#   - Add optional parameter for temporary file's extension
#   - Add fenced command for automatic fences around text
# Keith Smiley
# Version 1.0.1: Add configurable editor key
# Version 1.0.0: initial release

import os
import os.path
import subprocess

import weechat


def edit(data, buf, args, fenced=False):
    editor = (weechat.config_get_plugin("editor") or
              os.environ.get("EDITOR", "vim -f"))
    extension = "md" if not args else args
    path = os.path.expanduser("~/.weechat/message." + extension)
    open(path, "w+")

    cmd = editor.split() + [path]
    code = subprocess.Popen(cmd).wait()
    if code != 0:
        os.remove(path)
        weechat.command(buf, "/window refresh")
        return weechat.WEECHAT_RC_ERROR

    with open(path) as f:
        text = f.read()
        if fenced:
            text = "```\n" + text.strip() + "\n```"
        weechat.buffer_set(buf, "input", text)
        weechat.buffer_set(buf, "input_pos", str(len(text)))

    os.remove(path)
    weechat.command(buf, "/window refresh")

    return weechat.WEECHAT_RC_OK


def fenced(data, buf, args):
    return edit(data, buf, args, fenced=True)


def main():
    if not weechat.register("edit", "Francisco Lopes / Keith Smiley",
                            "1.1.0", "MIT",
                            "Open your $EDITOR to compose a message", "", ""):
        return weechat.WEECHAT_RC_ERROR

    weechat.hook_command("edit", "Open your $EDITOR to compose a message",
                         "[extension]",
                         "extension: extension for temporary composing file",
                         "extension", "edit", "")
    weechat.hook_command("fenced", "Open your $EDITOR to compose a message"
                                   " with automatic code fences",
                         "[extension]",
                         "extension: extension for temporary composing file",
                         "extension", "fenced", "")


if __name__ == "__main__":
    main()
